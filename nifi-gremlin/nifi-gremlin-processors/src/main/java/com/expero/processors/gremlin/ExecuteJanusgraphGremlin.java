/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.expero.processors.gremlin;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.nifi.annotation.lifecycle.OnShutdown;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.annotation.lifecycle.OnUnscheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.io.OutputStreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.stream.io.StreamUtils;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.MessageSerializer;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.driver.ser.GryoMessageSerializerV3d0;
import org.apache.tinkerpop.gremlin.groovy.jsr223.GremlinGroovyScriptEngine;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.io.gryo.GryoMapper;
import org.apache.tinkerpop.gremlin.structure.util.empty.EmptyGraph;
import org.janusgraph.graphdb.tinkerpop.JanusGraphIoRegistry;

import javax.script.Bindings;
import javax.script.SimpleBindings;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;


@Tags({"gremlin", "janusgraph", "executeTraversal"})
@CapabilityDescription("Executes a gremlin traversal when provided a gremlin traversal string. The result of the gremlin traversal is the output flowfile")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
@WritesAttributes({@WritesAttribute(attribute="", description="")})
public class ExecuteJanusgraphGremlin extends AbstractProcessor {

    public static final PropertyDescriptor GREMLIN_QUERY = new PropertyDescriptor
            .Builder().name("Gremlin Query")
            .displayName("Gremlin Query")
            .description("Gremlin Traversal Query")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    public static final PropertyDescriptor GREMLIN_BINDINGS = new PropertyDescriptor
            .Builder().name("Gremlin Bindings")
            .displayName("Gremlin Bindings")
            .description("Gremlin Traversal Parameter Bindings")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    //ipadresses
    public static final PropertyDescriptor GREMLIN_SERVERS = new PropertyDescriptor
            .Builder().name("Gremlin Servers")
            .displayName("Gremlin Servers")
            .description("Gremlin Server hostname/ipaddresses")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    //port#
    public static final PropertyDescriptor GREMLIN_SERVER_PORT = new PropertyDescriptor
            .Builder().name("Gremlin Server Port")
            .displayName("Gremlin Server Port")
            .description("Gremlin Server Port Number")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    //min pool
    public static final PropertyDescriptor GREMLIN_SERVER_MIN_POOL = new PropertyDescriptor
            .Builder().name("Minimum Connection Pool")
            .displayName("Minimum Connection Pool")
            .description("Minimum Connection Pool")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    //max pool
    public static final PropertyDescriptor GREMLIN_SERVER_MAX_POOL = new PropertyDescriptor
            .Builder().name("Maximum Connection Pool")
            .displayName("Maximum Connection Pool")
            .description("Maximum Connection Pool")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    //gtraversal
    public static final PropertyDescriptor GREMLIN_TRAVERSAL_SOURCE = new PropertyDescriptor
            .Builder().name("Gremlin Traversal Source")
            .displayName("Gremlin Traversal Source")
            .description("Gremlin Traversal Source")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    //user
    public static final PropertyDescriptor GREMLIN_SERVER_USER = new PropertyDescriptor
            .Builder().name("Gremlin Server User")
            .displayName("Gremlin Server User")
            .description("Gremlin Server User")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    //password
    public static final PropertyDescriptor GREMLIN_SERVER_PASSWORD = new PropertyDescriptor
            .Builder().name("Gremlin Server Password")
            .displayName("Gremlin Server Password")
            .description("Gremlin Server Password")
            .required(true)
            .sensitive(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    //relationships
    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("A Flowfile routed to this relationship have successfully been executed")
            .build();
    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("A FlowFile is routed to this relationship resulted in unsuccessful execution ")
            .build();


    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    private volatile Graph graph;  //volatile - will be in main memory and not in worker memory
    private volatile GraphTraversalSource g;
    private volatile GremlinGroovyScriptEngine engine;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(GREMLIN_QUERY);
        descriptors.add(GREMLIN_BINDINGS);
        descriptors.add(GREMLIN_SERVERS);
        descriptors.add(GREMLIN_SERVER_PORT);
        descriptors.add(GREMLIN_SERVER_MIN_POOL);
        descriptors.add(GREMLIN_SERVER_MAX_POOL);
        descriptors.add(GREMLIN_TRAVERSAL_SOURCE);
        descriptors.add(GREMLIN_SERVER_USER);
        descriptors.add(GREMLIN_SERVER_PASSWORD);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {

        try {
            graph = EmptyGraph.instance();
            final GryoMapper.Builder kryo = GryoMapper.build().addRegistry(JanusGraphIoRegistry.getInstance());
            final MessageSerializer serializer = new GryoMessageSerializerV3d0(kryo);
            final String strGremlinServers = context.getProperty(GREMLIN_SERVERS).evaluateAttributeExpressions().getValue();
            final Integer strGremlinServePort = context.getProperty(GREMLIN_SERVER_PORT).evaluateAttributeExpressions().asInteger();
            final Integer strGremlinMinPool = context.getProperty(GREMLIN_SERVER_MIN_POOL).evaluateAttributeExpressions().asInteger();
            final Integer strGremlinMaxPool = context.getProperty(GREMLIN_SERVER_MAX_POOL).evaluateAttributeExpressions().asInteger();
            final String strGremlinTraversalSource = context.getProperty(GREMLIN_TRAVERSAL_SOURCE).evaluateAttributeExpressions().getValue();
            final String strGremlinUser = context.getProperty(GREMLIN_SERVER_USER).evaluateAttributeExpressions().getValue();
            final String strGremlinPassword = context.getProperty(GREMLIN_SERVER_PASSWORD).evaluateAttributeExpressions().getValue();
            final Cluster.Builder builder = Cluster.build(strGremlinServers).port(strGremlinServePort)
                                                .serializer(serializer)
                                                .minConnectionPoolSize(strGremlinMinPool)
                                                .maxConnectionPoolSize(strGremlinMaxPool)
                                                .credentials(strGremlinUser, strGremlinPassword);
            final Cluster cluster = builder.create();
            g = graph.traversal().withRemote(DriverRemoteConnection.using(cluster, strGremlinTraversalSource));
            engine = new GremlinGroovyScriptEngine();
        }
        catch (Exception ex){
            getLogger().error(ex.getMessage());
        }
    }

    @OnShutdown
    public void onStopped(final ProcessContext context) {
        try {
            graph.close();
        }
        catch (Exception ex){
            getLogger().error(ex.getMessage());
        }

    }

     @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }
        // TODO implement
        final String strTraversal = context.getProperty(GREMLIN_QUERY).evaluateAttributeExpressions(flowFile).getValue();
        final String gremlinBindingsJson = context.getProperty(GREMLIN_BINDINGS).evaluateAttributeExpressions(flowFile).getValue();
        final ObjectMapper mapper = new ObjectMapper();
        final Bindings bindings;
        try {
            //copy json entries to bindings
            Map<String, String> map = mapper.readValue(gremlinBindingsJson, Map.class);
            bindings = new SimpleBindings();
            bindings.put("g", g);
            map.entrySet().stream().forEach(e -> {
                bindings.put(e.getKey(), e.getValue());
            });

            final GraphTraversal gTraversal = (GraphTraversal) engine.eval(strTraversal, bindings);
            final Object runResult = gTraversal.next();
            session.putAttribute(flowFile, "runResult", runResult.toString());
            getLogger().debug("runResult:"+ runResult.toString());
            flowFile = session.write(flowFile,(final OutputStream out) -> {
                out.write(runResult.toString().getBytes(StandardCharsets.UTF_8));
            });

            session.getProvenanceReporter().modifyAttributes(flowFile);
            session.transfer(flowFile, REL_SUCCESS);
        }
        catch (Exception ex){
            getLogger().error(ex.getMessage());
            session.transfer(flowFile, REL_FAILURE);
        }

    }
}
